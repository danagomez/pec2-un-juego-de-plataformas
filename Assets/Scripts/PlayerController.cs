using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    private static PlayerController instance;

    private static float velocitat;
    private static float forcaSalt;

    private static float currentX;

    private float multiplicadorCaiguda;
    private float multiplicadorSaltBaix;

    private bool isGrounded;
    private Transform isGroundedChecker;
    private float checkGroundRadius;
    [SerializeField] private LayerMask groundLayer;

    private static Animator playerAnimator;
    public static bool isDead;
    public static bool isSuper;

    void Start()
    {
        instance = this;
        playerAnimator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        isGroundedChecker = transform.Find("GroundChecker").GetComponent<Transform>();

        velocitat = 8f;
        forcaSalt = 9f;
        currentX = GetComponent<Transform>().localScale.x;
        multiplicadorCaiguda = 2.5f;
        multiplicadorSaltBaix = 2f;
        checkGroundRadius = 0.1f;
        isDead = false;
        isSuper = false;
    }

    void Update()
    {
        Moviment();
        Salt();
    }

    private void FixedUpdate()
    {
        CheckIfGrounded();

        if (isGrounded)
            if (Input.GetButton("Horizontal"))
                playerAnimator.Play("run");
            else
                playerAnimator.Play("idle");
        else
            playerAnimator.Play("jump");
    }

    void Moviment()
    {
        bool bothkeys = Input.GetKeyDown(KeyCode.RightArrow) && Input.GetKeyDown(KeyCode.LeftArrow);

        if (!bothkeys)
        {
            float x = Input.GetAxisRaw("Horizontal");
            Vector3 theScale = transform.localScale;

            if (x > 0)
                theScale.x = currentX * 1;
            else if (x < 0)
                theScale.x = currentX * -1;

            transform.localScale = theScale;

            rb.velocity = new Vector2(x * velocitat, rb.velocity.y);
        }
            
    }

    void Salt()
    {
        if (Input.GetButton("Jump") && isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, forcaSalt);
            if (isSuper)
                GameManager.audio[2].Play();
            else
                GameManager.audio[1].Play();
        }

        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity * (multiplicadorCaiguda - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
        {
            rb.velocity += Vector2.up * Physics2D.gravity * (multiplicadorSaltBaix - 1) * Time.deltaTime;
        }
    }

    void CheckIfGrounded()
    {
        Collider2D colliders = Physics2D.OverlapCircle(isGroundedChecker.position, checkGroundRadius, groundLayer);

        if (colliders != null)
            isGrounded = true;
        else
            isGrounded = false;
    }

    public static void BecomeSuper(bool becomeSuper)
    {
        Vector3 newScale = instance.transform.localScale;

        if (becomeSuper)
        {
            if (!isSuper)
            {
                GameManager.audio[8].Play();

                isSuper = true;
                velocitat += 1f;
                forcaSalt += 1f;

                newScale.x /= 0.8f;
                newScale.y /= 0.8f;
            }
        }
        else
        {
            isSuper = false;
            velocitat -= 1f;
            forcaSalt -= 1f;

            newScale.x *= 0.8f;
            newScale.y *= 0.8f;
        }

        currentX = newScale.x;
        instance.transform.localScale = newScale;
    }
}
