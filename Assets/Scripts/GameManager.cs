using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    private static GameObject player;
    private static Rigidbody2D playerRB;
    private static Animator playerAnimation;
    private static PlayerController playerController;

    private static float bounce;

    private GameObject pauseScreen;
    private bool paused;
    private static bool gameOver;
    private static Text endText;

    private static Text scoreText;
    private static Text coinsText;
    private static Text timeText;

    private static int score;
    public static int coins;
    private static string time;

    private static GameObject scorePrefab;

    public static AudioSource[] audio;
    // 0 -> Background Music
    // 1 -> Jump normal
    // 2 -> Jump super
    // 3 -> Bump block
    // 4 -> Break block
    // 5 -> Kick enemy
    // 6 -> Coin
    // 7 -> Powerup appears
    // 8 -> Powerup use
    // 9 -> Pause
    // 10 -> Game over
    // 11 -> Pole
    // 12 -> Victory

    void Start()
    {
        instance = this;
        Time.timeScale = 1f;

        player = GameObject.Find("Player");
        playerRB = player.GetComponent<Rigidbody2D>();
        playerAnimation = player.GetComponent<Animator>();
        playerController = player.GetComponent<PlayerController>();
        bounce = 10f;

        endText = GameObject.Find("Title").GetComponent<Text>();
        pauseScreen = GameObject.Find("PauseScreen");
        pauseScreen.SetActive(false);
        paused = false;
        gameOver = false;

        scoreText = GameObject.Find("TextScore").GetComponent<Text>();
        coinsText = GameObject.Find("TextCoins").GetComponent<Text>();
        timeText = GameObject.Find("TextTime").GetComponent<Text>();
        score = 0;
        coins = 0;
        time = string.Empty;

        scorePrefab = Resources.Load<GameObject>("Prefabs/ScoreCanvas");

        audio = GameObject.Find("Audio").GetComponents<AudioSource>();
    }

    private void Update()
    {
        if (!gameOver)
        {
            var ts = TimeSpan.FromSeconds(Time.timeSinceLevelLoad);
            time = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
            timeText.text = "Time\n" + time;

            scoreText.text = "Score\n" + score.ToString("000000");

            coinsText.text = "x " + coins.ToString("00");

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                pauseScreen.SetActive(!pauseScreen.activeSelf);
                Time.timeScale = 0f;
                paused = !paused;
                if (paused)
                {
                    playerController.enabled = false;
                    audio[0].Pause();
                    audio[9].Play();
                }
                else
                {
                    playerController.enabled = true;
                    audio[0].UnPause();
                }
            }
        }

        if (!paused)
            Time.timeScale = 1f;
    }

    public static void PlayerKillsEnemy(GameObject enemy)
    {
        playerRB.velocity = new Vector2(playerRB.velocity.x, bounce);

        EnemyDies(enemy);
    }

    public static void EnemyDies(GameObject enemy)
    {
        audio[5].Play();
        ScorePoints(enemy);

        Rigidbody2D enemyRB = enemy.GetComponent<Rigidbody2D>();

        enemyRB.velocity = new Vector2(enemyRB.velocity.x, bounce / 3f);

        enemy.GetComponent<PolygonCollider2D>().enabled = false;

        Animator enemyAnimation = enemy.GetComponent<Animator>();
        enemyAnimation.SetTrigger("EnemyDie");

        Transform enemyTransform = enemy.GetComponent<Transform>();
        enemyTransform.localScale = new Vector3(enemyTransform.localScale.x, 1.3f);

        Destroy(enemy, enemyAnimation.GetCurrentAnimatorStateInfo(0).length + 1);

    }

    public static void EndLevel()
    {
        playerController.enabled = false;
        gameOver = true;

        audio[0].Stop();

        instance.StopAllCoroutines();

        if (PlayerController.isDead)
            instance.StartCoroutine(instance.PlayerDies());
        else
            WinGame();
    }

    IEnumerator PlayerDies()
    {
        audio[10].Play();
        
        player.GetComponent<BoxCollider2D>().enabled = false;
        playerAnimation.SetTrigger("die");

        yield return new WaitForSeconds(playerAnimation.GetCurrentAnimatorStateInfo(0).length + 2);
        pauseScreen.SetActive(true);
        endText.text = "Game Over";
    }

    public static void ScorePoints(GameObject parent)
    {
        // Killing enemies: 100 pts
        // Getting powerup: 1000 pts
        // Getting coins: 10 pts

        int scoreGain = 0;
        if (parent.CompareTag("Enemy"))
            scoreGain = 100;
        else if (parent.CompareTag("Powerup"))
            scoreGain = 1000;
        else
            scoreGain = 10;

        score += scoreGain;
        GameObject tempCanvas = Instantiate(scorePrefab, parent.transform.position, parent.transform.rotation);
        Transform child = tempCanvas.transform.GetChild(0);
        child.gameObject.GetComponent<Text>().text = scoreGain.ToString();
        Destroy(tempCanvas, child.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + 1);
        
    }

    private static void WinGame()
    {
        instance.StartCoroutine(instance.MovePlayer());
        instance.StartCoroutine(instance.PlayAudio());
    }

    private IEnumerator PlayAudio()
    {
        audio[11].Play();
        yield return new WaitForSeconds(audio[11].clip.length);

        audio[12].Play();
        yield return new WaitForSeconds(audio[12].clip.length);

        pauseScreen.SetActive(true);
        endText.text = "Victory!";
    }

    private IEnumerator MovePlayer()
    {
        playerRB.velocity = new Vector2(3, playerRB.velocity.y);
        playerAnimation.Play("run");

        yield return new WaitForSeconds(4);
        playerRB.velocity = new Vector2(0, 0);
        playerAnimation.Play("idle");
    }
}
