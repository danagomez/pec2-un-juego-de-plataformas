using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private float followSpeed;
    public Transform Target;
    private Vector3 offset;
    private  float y;
    private float lastPos;
    private float rightBounds;

    private void Start()
    {
        followSpeed = 3f;
        y = transform.position.y;
        offset = Target.position - transform.position;
        lastPos = transform.position.x;
        rightBounds = 270f;
    }

    private void Update()
    {
        if ((Target.position.x - lastPos) > offset.x && Target.position.x < rightBounds)
        {
            Vector3 newPosition = Target.position - offset;
            newPosition.y = y;
            transform.position = Vector3.Slerp(transform.position, newPosition, followSpeed * Time.deltaTime);
            lastPos = transform.position.x;
        }
        
    }

}
