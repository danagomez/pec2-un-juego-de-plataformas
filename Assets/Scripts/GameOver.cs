using UnityEngine;

public class GameOver : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("End"))
            GameManager.EndLevel();
        else
            if (collision.CompareTag("Player"))
            {
                PlayerController.isDead = true;
                GameManager.EndLevel();
            }
        
    }
}
