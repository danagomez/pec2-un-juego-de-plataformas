using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    private static AudioSource buttonAudio;

    private void Start()
    {
        //buttonAudio = GameObject.Find("ButtonAudio").GetComponent<AudioSource>();
    }

    public void ReturnToMainMenu()
    {
        //buttonAudio.Play(0);
        SceneManager.LoadScene("MainMenu");
    }

    public void StartNewGame()
    {
        //buttonAudio.Play(0);
        SceneManager.LoadScene("Game");
    }

    public void ExitGame()
    {
        //buttonAudio.Play(0);
        Debug.Log("Sortint del joc...");
        Application.Quit();
    }

}
