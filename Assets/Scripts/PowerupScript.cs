using UnityEngine;

public class PowerupScript : MonoBehaviour
{
    private Rigidbody2D rb;
    private float velocitat;

    public bool goingLeft;

    void Start()
    {
        GameManager.audio[7].Play();

        rb = GetComponent<Rigidbody2D>();
        goingLeft = false;
        velocitat = 3f;

        Vector3 newPos = transform.position;
        newPos.y += 2f;
        transform.position = newPos;
    }

    private void FixedUpdate()
    {
        MovePowerup();
    }

    private void MovePowerup()
    {
        float x = 0;
        if (goingLeft)
            x = -1;
        else
            x = 1;

        rb.velocity = new Vector2(x * velocitat, rb.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 direction = collision.GetContact(0).normal;

        if (collision.GetContact(0).collider.CompareTag("Player"))
        {
            if (!PlayerController.isSuper)
                PlayerController.BecomeSuper(true);

            GameManager.ScorePoints(gameObject);
            Destroy(gameObject);
        }
        else
        {
            if (direction.x == 1)
                goingLeft = false;
            else if (direction.x == -1)
                goingLeft = true;
        }
        
    }
}
