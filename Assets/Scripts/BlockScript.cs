using UnityEngine;

public class BlockScript : MonoBehaviour
{
    private bool isSpecial;
    private bool used;

    private bool enemyOnTop;
    private GameObject enemyObject;

    private Animator blockAnimation;

    private GameObject powerupPrefab;
    private GameObject coinPrefab;

    void Start()
    {
        enemyOnTop = false;
        blockAnimation = GetComponent<Animator>();
        powerupPrefab = Resources.Load<GameObject>("Prefabs/Mushroom");
        coinPrefab = Resources.Load<GameObject>("Prefabs/Coin");
        used = false;

        if (tag == "BlockSpecial")
            isSpecial = true;
        else
            isSpecial = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 direction = collision.GetContact(0).normal;

        if (direction.y == 1)
        {
            if (collision.GetContact(0).collider.CompareTag("Player"))
            {
                if (isSpecial && !used)
                {
                    GameManager.audio[3].Play();
                    blockAnimation.SetTrigger("BlockBounce");

                    blockAnimation.SetTrigger("BlockUsed");
                    used = true;

                    // There is a 70% chance of getting a powerup, otherwise you get a coin
                    if (Random.Range(0, 101) < 70)
                        GetPowerup();
                    else
                        GetCoin();
                    
                }
                else if (!isSpecial)
                {
                    if (enemyOnTop)
                        GameManager.EnemyDies(enemyObject);

                    blockAnimation.SetTrigger("BlockBounce");

                    // There is a 70% chance of getting a coin
                    if (!used && (Random.Range(0, 101) < 70))
                        GetCoin();

                    used = true;

                    //If player is super, block gets destroyed
                    if (PlayerController.isSuper)
                    {
                        GameManager.audio[4].Play();
                        Destroy(transform.parent.gameObject, 0.5f);
                    }
                    else
                        GameManager.audio[3].Play();
                }

            }
        }
        else if (direction.y == -1)
        {
            if (collision.GetContact(0).collider.CompareTag("Enemy"))
            {
                enemyOnTop = true;
                enemyObject = collision.gameObject;
            }
        }

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            enemyOnTop = false;
        }
    }


    void GetPowerup()
    {
        GameObject powerup = Instantiate(powerupPrefab, transform, true);
        powerup.transform.parent = transform;
        powerup.transform.localPosition = new Vector2(0, 0);
        powerup.transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    void GetCoin()
    {
        GameManager.audio[6].Play();

        GameObject coin = Instantiate(coinPrefab, transform, true);
        coin.transform.parent = transform;
        coin.transform.localPosition = new Vector2(0, 0);
        coin.transform.rotation = Quaternion.Euler(0, 0, 0);
        coin.GetComponent<Animator>().SetTrigger("CoinFlip");
        Destroy(coin, coin.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);

        GameManager.coins += 1;
        GameManager.ScorePoints(gameObject);
    }
}
