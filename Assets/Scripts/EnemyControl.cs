using UnityEngine;

public class EnemyControl : MonoBehaviour
{

    private Rigidbody2D rb;
    private float velocitat;

    private class EnemyState
    {
        public bool goingLeft = false;
        public bool grounded = false;
        public bool dead = false;
    }

    private EnemyState enemyState;

    private Transform isGroundedChecker;
    private float checkGroundRadius;
    [SerializeField] private LayerMask groundLayer;

    void Start()
    {
        enemyState = new EnemyState();
        enemyState.goingLeft = true;
        rb = GetComponent<Rigidbody2D>();
        enabled = false;
        velocitat = 3f;
        checkGroundRadius = 0.1f;
        isGroundedChecker = transform.Find("GroundChecker").GetComponent<Transform>();
    }

    void FixedUpdate()
    {
        CheckIfGrounded();
        UpdateEnemyPosition();
    }

    void UpdateEnemyPosition()
    {
        if (!enemyState.dead)
        {
            Vector2 scale = transform.localScale;

            if (enemyState.grounded)
            {
                float x = 0;

                if (enemyState.goingLeft)
                {
                    x = -1;
                    scale.x = 2.5f;
                }
                else
                {
                    x = 1;
                    scale.x = -2.5f;
                }

                rb.velocity = new Vector2(x * velocitat, rb.velocity.y);
            }
            transform.localScale = scale;
        }
    }

    void CheckIfGrounded()
    {
        Collider2D colliders = Physics2D.OverlapCircle(isGroundedChecker.position, checkGroundRadius, groundLayer);

        if (colliders != null)
        {
            enemyState.grounded = true;
        }
        else
        {
            enemyState.grounded = false;
        }
    }

    private void OnBecameVisible()
    {
        enabled = true;
    }

    private void OnBecameInvisible()
    {
        enabled = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 direction = collision.GetContact(0).normal;

        if (collision.GetContact(0).collider.CompareTag("Player"))
        {
            if (direction.y == -1)
            {
                enemyState.dead = true;
                GameManager.PlayerKillsEnemy(gameObject);
            }
            else
            {
                if (PlayerController.isSuper)
                {
                    PlayerController.BecomeSuper(false);
                    if (direction.x > 0 && direction.x <= 1)
                        enemyState.goingLeft = !enemyState.goingLeft;
                }
                else
                {
                    PlayerController.isDead = true;
                    GameManager.EndLevel();
                }
                
            }
        }
        else if (collision.GetContact(0).collider.CompareTag("Enemy"))
        {
            enemyState.goingLeft = !enemyState.goingLeft;
        }
        else
        {
            if (direction.x == 1)
                enemyState.goingLeft = false;
            else if (direction.x == -1)
                enemyState.goingLeft = true;

        }
        
    }

}
