
# PEC 2 - Un Juego de Plataformas  
  
Aquest joc est una recreació del primer nivell del joc per la NES "Super Mario Bros". Ha sigut creat per a un projecte del curs de Programació de Videojocs 2D de la UOC.  
  
Clica [aquí](https://youtu.be/tu8gJFrWriQ) per un vídeo de demostració del joc.  
  
Clica [aquí](https://danagomezbalada.itch.io/pec-2-super-mario-bros) per jugar a la versió de WebGL online.  

## Funcionalitats del joc  
  
Un cop executem el joc, veurem la pantalla inicial. En aquesta pantalla veurem dos botons:  
- **Nova partida**: Ens portarà a la *scene* principal del joc.  
- **Sortir del joc**: Es tancarà el joc.  
  
També veurem a la part superior el **títol** del joc. Podem navegar per aquest menú amb el teclat i el rató.  
  
![Foto del menú inicial](https://gitlab.com/danagomez/pec2-un-juego-de-plataformas/-/raw/master/Images/MainMenu.png)
  
Un cop comença el joc principal, escoltarem la icònica **música de SMB** i veurem a un personatge per davant d'un paisatge de naturalesa. Al fons veurem unes muntanyes i núvols, al front veurem el terra.  
A la **part superior** podem veure tres marcadors:  
- **Puntuació**: És la puntuació que el jugador ha acumulat. Té un máxim de **6 dígits** i s'actualitza automàticament a mesura que s'obtenen punts. Podem obtenir punts:  
	- *Matant enemics*  
	- *Obtenint powerups*  
	- *Obtenint monedes*  
- **Monedes**: És el total de monedes obtingudes. Té un máxim de **2 dígits** i s'actualitza automàticament a mesura que s'obtenen monedes. Podem obtenir monedes colpejant o destruïnt *blocs*.  
- **Temps**: És el contador del temps emprat en la partida actual. S'actualitza automàticament i mostra els **segons i minuts**.  
  
![Foto inicial del joc](https://gitlab.com/danagomez/pec2-un-juego-de-plataformas/-/raw/master/Images/StartGame.png) 
  
Podem moure el personatge de la següent manera:  
- **Moviment horitzontal**: prement les tecles "*A*" o "*D*", o bé les fletxes *d'esquerra* i *dreta*.  
- **Salt**: prement la tecla *d'espai*. Si **mantenim** la tecla presionada el personatge saltará una mica més alt. També podem mouren's **horitzontalment** mentre saltem.  
  
La **cámara** segueix al jugador sempre i quan aquest avançi (cap a la dreta), si intenta tornar enrere la cámara no es  mourá. A més, el jugador **no pot tornar enrere** més enllà del límit de la cámara.  
  
A mesura que anem avançant ens trobarem amb varis *blocs*, *enemics* i *obstacles*. Hi ha dos tipus de **blocs**:  
- **Blocs normals**: Aquests blocs reboten quan el jugador els colpeja per sota, i si el jugador es troba en estat super es trenquen. Aquests blocs tenen un **70%** de probabilitat de donar una **moneda** al jugador (si no, no donen res).  
- **Blocs especials**: Aquests blocs són de color groc i reboten al ser utilitzats per primer cop, un cop han sigut utilitzats no reboten més. Aquests blocs tenen un **70%** de probabilitat de donar un ***powerup*** (si no, donen una *moneda*).  
	- **Monedes**: Són les monedes que poden sortir dels blocs normals o especials, i el jugador els pot col·leccionar. Cada cop que s'obté una moneda **incrementa el comptador de monedes** i el jugador **guanya 10 punts**.  
	- **Powerup**: És una seta que pot sortir dels blocs especials. Un cop surt, **es mou automàticament** cap a la dreta, i si col·lisiona amb algún objecte (a excepció del jugador) va cap a la direcció oposada.  
		- Si el **jugador toca el *powerup***, aquest passa a estat **súper** (si no ho era ja). En aquest estat, el seu **tamany incrementa**, així com la **velocitat** i la **força de salt**. A més, si toca un enemic (no saltant a sobre), es perd l'estat súper però **no es mor**. Per cada *powerup* que el jugador obtingui, **guanya 1000 punts**.  
  
Els **enemics** es mouen **horitzontalment**, i si col·lisionen amb l'entorn o altres enemics **canvien de direcció**. Cada cop que el jugador mati a un enemic **obtindrá 100 punts**. El jugador pot matar enemics:  
- ***Saltant a sobre***: quan el jugador salta sobre un enemic, aquest rebota i l'enemic serà aixafat i caurà pel terra.  
- ***Colpejant un bloc normal si hi ha un enemic a sobre***: si hi ha un enemic sobre un bloc normal, el jugador té l'oportunitat de matar-lo per sota colpejant el bloc.  
  
![Foto dels blocs i enemics](https://gitlab.com/danagomez/pec2-un-juego-de-plataformas/-/raw/master/Images/Game.png)
  
També hi ha una sèrie d'**obstacles** (com ara *caixes*) que el jugador ha de superar per poder avançar. Uns obstacles notables són els **forats del terra**, ja que si el jugador cau per aquests **morirà** (ja sigui súper o no).  
  
Un cop arribem al **final del joc**, veurem una bandera i un castell. Un cop passem per la **barra metàl·lica** de la bandera, veurem una petita ***cutscene*** on el jugador és mourà automàticament cap al castell. Escoltarem una **música de victòria** i en uns segons apareixerà un **menú** on podrem escollir si tornar al menú inicial o jugar de nou. A la part superior podrem veure els **punts i monedes totals**, i el **comptador de temps** és congelarà.  
  
![Foto del final del joc](https://gitlab.com/danagomez/pec2-un-juego-de-plataformas/-/raw/master/Images/Victory.png)

A més, si premem el botó '***Esc***' durant el joc sortirà una **pantalla de pausa** on podrem tornar al menú inicial o reiniciar la partida. El joc i la música estaran pausats mentre el menú de pausa sigui actiu.  
  
## Explicació del codi  
  
### SceneChanger.cs  
Aquest script s'encarrega de controlar els **canvis d'escena**.  
  
### CameraScript.cs  
Aquest script, assignat a la càmera del joc, s'encarrega de **seguir al jugador** sempre i quan aquest avanci. El seguiment de la càmera és fluid gràcies al mètode ***Slerp*** de *Vector3*. A més, quan el jugador arriba al final del joc, la càmera **deixarà de moure's** (el final del joc està indicat per la variable ***rightBounds***, tot i que també podríem haver utilitzat un *Trigger*).  
  
### PlayerController.cs  
Aquest script s'encarrega principalment del **moviment i la lògica** del jugador. També controla l'**estat actual del jugador** en tot moment. Les funcions principals d'aquest script són:  
- Comprovar si el jugador està **tocant el terra**. Fent servir el *Transform* d'un objecte buit dins del jugador i el *layer* del terra.  
- Comprovar si estem prement alguna tecla.  
	- **Moviment horitzontal**: Podem moure el jugador polsant les tecles de moviment horitzontal (A o D, o bé fletxa esquerra o dreta). El sprite del jugador apuntarà a la direcció cap a la qual s'està movent.  
	- **Salt**: si el jugador està tocant el terra, podem polsar la tecla espai per saltar. Si la mantenim polsada el jugador saltarà una mica més alt.  
- **Convertir al jugador** en estat súper i tornar-lo de volta a l'estat normal. Fent servir un mètode que és cridarà cada cop que el jugador toqui un powerup o un enemic. Si el jugador està en estat súper, es torna a estat normal, si no es converteix en estat súper.  
- Controlar l'**estat actual** del jugador, amb els booleans ***isGrounded***, ***isDead*** i ***isSuper***. Aquests dos últims són de tipus public i static per poder accedir-los des d'altres scripts.  
- Controlar les **animacions** del personatge del jugador. Depenent de l'acció actual, l'animació canviarà (quiet, corrent o saltant).  
  
### EnemyControl.cs  
Aquest script s'encarrega de controlar el **moviment i la lògica** dels enemics. També controla l'**estat actual** de cada enemic en tot moment. Les funcions principals d'aquest script són:  
- Comprovar si l'enemic està **tocant el terra**. Fent servir el *Transform* d'un objecte buit dins de l'enemic i el *layer* del terra.  
- **Moure l'enemic automàticament**. Inicialment, l'enemic es mourà cap a l'**esquerra** i, si col·lisiona amb algun obstacle o altres enemics, **canviarà de direcció**. Si l'enemic no està tocant el terra no podrà avançar horitzontalment.  
- Controlar l'**estat actual** de l'enemic fent servir la classe ***EnemyState***, en la qual tenim booleans d'estat: ***goingLeft***, ***grounded*** i ***dead***.  
- **Controlar les col·lisions**:  
	- Si l'enemic col·lisiona amb el **jugador**, comprova si la col·lisió és **per sobre** (per tant, el jugador ha saltat sobre l'enemic). Si aquest és el cas, l'estat ***dead*** de l'enemic serà *true* i cridarà el mètode del *GameManager* per **matar l'enemic**. Si no, **mata al jugador** canviant el seu estat i cridant el mètode ***EndLevel*** del *GameManager* (si està en estat **súper**, crida el mètode de *PlayerController* per tornar-lo normal i canvia de direcció per no col·lisionar un altre cop).  
	- Si l'enemic col·lisiona amb un altre **enemic**, canvia de direcció.  
	- Si l'enemic col·lisiona amb **qualsevol altra cosa**, anirà a la direcció contrària a la col·lisió.  
- Addicionalment, si l'enemic no és visible és **desactivarà** (per tant, no es podrà moure fins que sigui **visible**).  
  
### BlockScript.cs  
Aquest script s'encarrega de controlar la **lògica dels blocs**, així com la **creació de powerups i monedes**. Ja que aquest script controla tots els blocs, fem servir un booleà ***isSpecial*** per distingir el tipus de bloc.  
  
Les funcions principals d'aquest script es fan a través del ***OnCollisionEnter2D***, ja que els blocs no fan res mentre el jugador no col·lisioni amb ells.  
- **Si la col·lisió és per sota**: Comprova que hagi sigut el **jugador**, i si ho ha sigut, farà una acció diferent depenent de si és especial (i no utilitzat) o normal.  
	- **Si és especial**: El bloc rebotarà i serà utilitzat (canviant el color del bloc). Hi ha un **70%** de probabilitat de què surti un ***powerup***, si no, sortirà una *moneda*. Un cop el bloc especial s'ha utilitzat no farà res més.  
	- **Si és normal**: El bloc rebotarà i comprovarà si hi ha un enemic a sobre (utilitzant el booleà ***enemyOnTop***), si aquest és el cas l'enemic morirà. Hi ha un **70%** de probabilitat de que surti una **moneda**, si no, no sortirà res. Si el jugador està en estat **súper**, el bloc es destruirà.  
- **Si la col·lisió és per sobre**: Comprova si ha sigut un enemic, i si aquest és el cas, guardarà aquesta informació utilitzant el booleà ***enemyOnTop*** i guardant el Game Object de l'enemic.  
  
També detecta si l'enemic ha deixat d'estar a sobre utilitzant el mètode ***OnCollisionExit2D***.  
  
Per tal de crear els powerups i les monedes, utilitza mètodes diferents:  
- **GetPowerup()**: Crea l'objecte de ***powerup*** (trobat a la carpeta de *Prefabs* dintre de *Resources*) i el posiciona en el **centre** del bloc d'on ha sortit.  
- **GetCoin()**: Crea l'objecte de **moneda** (trobat a la carpeta de *Prefabs* dintre de *Resources*) i executa la seva animació. Un cop acaba l'animació **destrueix** l'objecte. A més, **suma un punt** al comptador de monedes del ***GameManager*** i crida el seu mètode per afegir puntuació.  
  
### PowerupScript.cs  
Aquest script s'encarrega de controlar el **moviment i la lògica** dels powerups. Un cop es crea l'objecte, es posiciona més amunt (per tal de no col·lisionar amb el bloc del qual ha sortit) i comença a **moure's cap a la dreta**.  
  
Si col·lisiona amb el **jugador**, comprova si el jugador no està en estat **súper** i, de ser així, crida el mètode del ***PlayerController*** per convertir-lo. Després, crida el mètode del ***GameManager*** per afegir puntuació i és **destrueix**.  
Si col·lisiona amb **qualsevol altra cosa**, canvia de direcció.  
  
### GameManager.cs  
Aquest script s'encarrega de controlar alguns dels **aspectes principals** del joc, així com els **elements de l'UI**, els **comptadors** i l'**àudio**. Les funcions d'aquest script són:  
- Controlar el "**fi del joc**" (en el qual la música de fons és *pararà*, el script del jugador és *deshabilitarà* i els comptadors és *detindran*), amb totes les seves possibilitats:  
	- Si l'**estat del jugador** és **isDead**, executa un àudio de *game  over* i executa l'animació del jugador per **morir**. Després, mostra la pantalla de pausa amb el text de "***Game Over***".  
	- Si no, cridarà **dos mètodes**: un per executar la **música de final** (primer executa el so de la barra de metall i després la música de victòria) i, un cop acabada la música, mostrar la pantalla de pausa amb el text de ***Victory***; l'altre mètode s'encarrega de **moure el personatge del jugador cap a la dreta** automàticament durant **4 segons** (executant l'animació de ***run*** mentre es mou, i la ***idle*** quan pari).  
- Controlar **si el jugador mata a l'enemic**. Quan el jugador mata a un enemic, aquest **rebotarà** (sempre i quan l'hagi matat saltant a sobre).  
Quan l'enemic mor, farà un **rebot petit**, **caurà pel terra** (ja que es deshabilita el seu collider) i és **destruirà l'objecte**. També **s'afegiran els punts corresponents** cridant al mètode per afegir puntuació.  
- Controlar els **comptadors** de *puntuació*, *monedes* i *temps*.  
	- **Puntuació**: depenent del tag del *Game Object* que rep com a paràmetre a través del mètode ***ScorePoints*** sumarà una quantitat de punts diferent (**Enemic**: 100 punts; **Powerup**: 1000 punts; **Moneda**: 10 punts). A més, cada cop que s'aconsegueixin punts, **mostrarà un text** (fent servir un *canvas* de tipus ***World  Space*** com a *parent*, trobat al *Prefabs*) indicant la **quantitat de punts** obtinguda al costat de l'objecte del qual hem obtingut els punts. Després, destrueix l'objecte d'aquest text.  
	- **Monedes**: En principi, aquest script només s'encarrega d'**actualitzar el text** que mostra el total de monedes, ja que fem servir una variable **pública** pel comptador de monedes, de manera que s'acumulen a través del **script dels blocs**.  
	- **Temps**: Mostra el **temps que ha passat des del principi** de la partida, en format **mm:ss**.  
- Controlar el **menú de pausa**. Si el jugador pressiona la tecla de "***Esc***", és **detindrà** el temps i la música i és **mostrarà** el menú de pausa.  
- Controlar l'**àudio**. Fent servir un **array** de ***AudioSource*** que serà **públic** (per tant, es pot controlar l'àudio des de qualsevol script). Els diferents àudios disponibles són:  
	- 0 -> Background  Music  
	- 1 -> Jump normal  
	- 2 -> Jump  super  
	- 3 -> Bump  block  
	- 4 -> Break block  
	- 5 -> Kick  enemy  
	- 6 -> Coin  
	- 7 -> Powerup  appears  
	- 8 -> Powerup  use  
	- 9 -> Pause  
	- 10 -> Game over  
	- 11 -> Pole  
	- 12 -> Victory  
  
### GameOver.cs  
Aquest script controla el ***Trigger*** de l'objecte de ***Game Over*** (posicionat a la part inferior de la càmera) i de l'objecte de ***End Game*** (la barra de metall de la bandera del final).  
Serveix per **comprovar** si el jugador **cau per un forat** o si ha **arribat al final**. En el primer cas, és canviarà l'estat del jugador a ***isDead*** i és cridarà el mètode de ***EndLevel*** del ***GameManager***. En el segon cas, només és cridarà el mètode de ***EndLevel***.  
  
## Assets utilitzats  
  
### Sprites:  
##### Player - https://blog.naver.com/ggogono3/222071785089  
##### Platformer Art Deluxe: https://opengameart.org/content/platformer-art-deluxe  
  
### Backgrounds:  
##### Background Elements Deluxe: https://www.kenney.nl/assets/background-elements-redux  
  
### Music:  
  
##### Super Mario Bros - https://downloads.khinsider.com/game-soundtracks/album/super-mario-bros  
  
### Sounds:  
  
##### Super Mario Bros Sound  Effects - https://themushroomkingdom.net/media/smb/wav